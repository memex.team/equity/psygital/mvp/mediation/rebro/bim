# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

require_relative 'lib/version'

Gem::Specification.new do |spec|
  spec.name          = "reBro_bim"
  spec.version       = ReBro::VERSION
  spec.authors       = ["Eugene Istomin"]
  spec.email         = ["e.istomin@memex.team"]

  spec.summary       = %q{'Bro'}
  spec.description   = %q{'Bro' is a bridge in Swedish. Re-Bro is about re-bridging, makes bridges again}
  spec.homepage      = "https://memex.team"
  spec.required_ruby_version = Gem::Requirement.new(">= 3.1.0")

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/memex.team/equity/archestry/digital/mediation/re-bro"
  spec.metadata["changelog_uri"] = "https://gitlab.com/memex.team/equity/archestry/digital/mediation/re-bro"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
