#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######

set -e 


inotifywait -m --exclude './_bin/mdbx.*|./6_qc/gauge/*|^\./bim/db/*|./*.kate-swp|./\.git' -r -e close_write --format '%w %f' ./ |\
(
while read line; do
  clear && systemctl --user --no-block restart reBro_bim-$1@$2
  echo "$1 - $2 Restarted at $(date +%s) by $line"
done
)  
