# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Grape
  module DSL
    module InsideRoute
      extend ActiveSupport::Concern

      def result(hIn)
        st = hIn.key?(:status) ? hIn[:status] : 200
        status(st)
        contentType = hIn.key?(:mime) ? hIn[:mime] : 'application/json'
        content_type(contentType)
        hIn[:res] = 'empty' unless hIn.key?(:res)

        unless hIn[:log] == false

          # Log::App.debug(hIn[:env]['PATH_INFO'])
          key = hIn[:env]['PATH_INFO'].split('/').drop(2).join(':')
          a = []
          hIn[:env]['rack.request.query_hash'].sort.each do |k, v|
            a << "#{k}=#{v}"
          end
          key = key + '::' + a.join('::')
          MDBX::Database.open(::Env::Base::Store.local._path, ::Env::Base::Store.local._opts) do |localStorage|
            localStorage.collection('lastcalls')
            localStorage.transaction do
              localStorage[key] = hIn[:res].to_json
            end
          end
        end
        hIn[:res]
      end

      def lastcall(env)
        key = env['PATH_INFO'].split('/').drop(2).join(':')
        a = []
        res = false
        query = env['rack.request.query_hash']
        query.delete('showlog')
        query.sort.each do |k, v|
          a << "#{k}=#{v}"
        end
        key = key + '::' + a.join('::')
        MDBX::Database.open(::Env::Base::Store.local._path, ::Env::Base::Store.local._opts) do |localStorage|
          localStorage.collection('lastcalls')
          res = Oj.load(localStorage.get(key))
        end
        #
        # Log::App.debug(key)
        res
      end
    end
  end
end
