# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Grape
  class StreamGenCSVSimple
    def initialize(hIn)
      @out = Grape.genCSVSimple(data: hIn[:data])
    end

    def each
      yield(@out)
    end
  end

  def self.genCSVSimple(hIn)
    hIn[:data].join("\n")
  end

  class StreamGenCSV2XLSX
    def initialize(hIn)
      @out = Grape.genCSV2XLSX(data: hIn[:data])
    end

    def each
      yield(@out)
    end
  end

  def self.genCSV2XLSX(hIn)
    hIn[:data].join("\n")
  end

  class StreamBinary
    def initialize(data)
      @out = Grape.StreamBinary(data)
    end

    def each
      yield(@out)
    end
  end

  def self.StreamBinary(data)
    data
  end
end
