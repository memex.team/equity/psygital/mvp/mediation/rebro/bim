# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
require 'date'
require 'oj'

def tsNow
  DateTime.now.strftime('%Q').to_i
end

class OkMain
  class << self # :nodoc:
    alias allocate new
    alias k new
  end

  RANGE = ((48..57).to_a + (65..90).to_a + (97..122).to_a)
  RANGE << 95

  InspectKey = :__inspect_key__ # :nodoc:
  def initialize(hash = {})
    @binding = ::Ok.pure_binding
    hash.each do |k, v|
      kObjString = k[0] == '_' ? k : "_#{k}"
      kObjString = nonAsciiTransform2Ascii(kObjString) if kObjString.match(/\W/)
      kObj = kObjString.to_sym

      if v.respond_to?(:keys)
        @binding.local_variable_set(kObj, ::Ok.new(v))
      # @binding.local_variable_set(kObj, v) ## IF RAW HASH IS NEEDED
      elsif v.respond_to?(:bsearch)
        a = []
        v.each do |values|
          valuesRedef =
            if values.respond_to?(:keys)
              @binding.local_variable_set(kObj, ::Ok.new(values)) ## SLOWDOWN HERE
            else
              values
            end
          a << valuesRedef
        end
        @binding.local_variable_set(kObj, a)
      else
        @binding.local_variable_set(kObj, v)
      end
    end
    @binding.local_variables.each do |name|
      get = -> { @binding.local_variable_get(name) }
      set = ->(new_var) { @binding.local_variable_set(name, new_var) }
      define_singleton_method(name, get)
      define_singleton_method("#{name}=".to_sym, set)
    end
  end

  def self.pure_binding
    binding
  end

  def nonAsciiTransform2Ascii(kObjString)
    res = ''
    kObjString.each_char do |char|
      res = RANGE.include?(char.ord) ? res + char : res + "ORD__#{char.ord}__ORD"
    end
    # Log::App.debug(false, res)
    res
  end

  def asciiTransform2NonAscii(kObjString)
    kObjString.gsub(/ORD__.*.__ORD/) { |word| word.split('__')[1].to_i.chr }
    # Log::App.debug(false, "asciiTransform2NonAscii: #{kObjString} -> #{res}")
  end

  def freeze
    singleton_methods.each do |method|
      instance_eval("undef #{method}", __FILE__, __LINE__) if method.to_s[-1] == '='
    end
    eval('def self.frozen?; true end')
    super
  end
  alias freeze! freeze

  def method_missing(name, arg = nil, valid = false)
    name.is_a?(Integer) ? true : name = name.to_sym
    if @binding.local_variables.include?(name)
      @binding.local_variable_get(name)

    elsif name.to_s[-1] == '=' && frozen?
      raise("can't modify frozen object")

    elsif name.to_s[-1] == '='
      pure_name = name.to_s.delete('=').to_sym
      @binding.local_variable_set(pure_name, arg)
      unless methods.include?(pure_name)
        set = ->(new_var) { @binding.local_variable_set(pure_name, new_var) }
        get = -> { @binding.local_variable_get(pure_name) }
        define_singleton_method(name, set)
        define_singleton_method(pure_name, get)
      end

    elsif name[0] == '_' || valid == true
      unless methods.include?(name)
        set = ->(new_var) { @binding.local_variable_set(name, new_var) }
        get = -> { @binding.local_variable_get(name) }
        define_singleton_method(name, set)
        define_singleton_method(name, get)
      end
      @binding.local_variable_set(name, ::Ok.new({}))
    else
      err = NoMethodError.new("undefined method `#{name}' for #{self}", name, [arg])
      err.set_backtrace(caller(1))
      raise(err)
    end
  end

  def [](name)
    key = name[0] == '_' ? name : "_#{name}"
    key = nonAsciiTransform2Ascii(key) if key.to_s.match(/\W/)
    method_missing(key, nil, true)
  end

  def []=(name, val)
    key = name[0] == '_' ? name : "_#{name}"
    key = nonAsciiTransform2Ascii(key) if key.to_s.match(/\W/)
    method_missing("#{key.to_sym}=", val)
  end

  def first
    table = ::Ok.new
    i = 0
    @binding.local_variables.each do |k|
      if i == 0
        table._k = k
        table._v = @binding.local_variable_get(k)
      end
      i += 1
    end
    # return to_enum(__method__) { table.size } unless block_given?
    table
  end

  def respond_to_missing?(method_name, include_private = false)
    last_char_name = method_name.to_s[-1]
    if last_char_name == '=' && frozen?
      false
    elsif last_char_name == '=' && @binding.local_variable_defined?(method_name.to_s.tr('=', ''))
      true
    else
      (@binding.local_variables.include?(method_name) && !@binding.local_variable_get(method_name)) || super
    end
  end

  def inspect
    str = "#<#{self.class}"

    ids = (Thread.current[InspectKey] ||= [])
    return str << ' ...>' if ids.include?(object_id)

    ids << object_id
    begin
      first = true
      @binding.local_variables.sort.each do |k|
        v = @binding.local_variable_get(k)
        str << ',' unless first
        first = false
        str << " #{k}=#{v.inspect}"
      end
      str << '>'
    ensure
      ids.pop
    end
  end
  alias to_s inspect

  def each(&)
    table = {}
    @binding.local_variables.each do |k|
      # Log::App.debug(false, k.to_s)
      kRead = k[0] == '_' ? k[1..-1] : k.to_s
      kRead = asciiTransform2NonAscii(kRead) if  kRead.include?('ORD__')
      table[kRead] = @binding.local_variable_get(k)
    end
    return to_enum(__method__) { table.size } unless block_given?

    table.each(&)
  end
  alias eachObj each

  def to_h
    output = {}
    @binding.local_variables.each do |var|
      next if var == :hash

      key = var[0] == '_' ? var[1..-1] : var
      key = asciiTransform2NonAscii(key) if key.include?('ORD__')
      case @binding.local_variable_get(var)
      when O, Ok
        output[key] = @binding.local_variable_get(var).to_h
      when Array
        a = []
        @binding.local_variable_get(var).each do |array|
          # array.class == Array ? a << array : a << array
          a << array
        end
        output[key] = a
      when Integer, Float
        output[key] = @binding.local_variable_get(var)
      else
        output[key] = @binding.local_variable_get(var).to_s
      end
    end
    output
  end
  alias to_hs to_h

  def to_json(*_args)
    Oj.dump(to_h)
  end

  def ==(other)
    return false unless other.is_a?(Ok)

    to_h == other.to_h
  end

  def sortCanvas
    output = {}
    @binding.local_variables.sort { |k| output[k] = @binding.local_variable_get(k) }
    output
  end

  def eql?(other)
    return false unless other.is_a?(Ok)

    to_h.eql?(other.to_h)
  end

  def key?(key)
    # Log::App.debug(false, "Key 1: #{key}")
    # Log::App.debug(false, "Key0: #{key[0]}")
    # Log::App.debug(false, "getKeys: #{@binding.local_variables}")
    key = nonAsciiTransform2Ascii(key) if key.to_s.match(/\W/)
    # Log::App.debug(false, "Key 2: #{key}")
    key = key[0] == '_' ? key.to_sym : "_#{key}".to_sym

    # Log::App.debug(false, "Key MOD: #{key}")
    @binding.local_variables.include?(key) ? true : false
    # Log::App.debug(false, "## RES: #{res}")
  end

  def getKeys
    keys = []
    @binding.local_variables.each do |k|
      keys << k
    end
    keys
  end
  alias keys getKeys

  def deleteKey(key)
    key = key[0] == '_' ? key.to_sym : "_#{key}".to_sym
    bindingPrev = @binding.dup
    @binding = ::Ok.pure_binding
    bindingPrev.local_variables.each do |var|
      next if var == key

      @binding.local_variable_set(var, bindingPrev.local_variable_get(var))
    end

    instance_eval("undef #{key}=", __FILE__, __LINE__)
    instance_eval("undef #{key}", __FILE__, __LINE__)

    bindingPrev.local_variable_get(key)
  end

  def count
    getKeys.count
  end

  def now
    method_missing('_now=', tsNow)
    tsNow
  end

  # def to_id name
  # method_missing("#{name.gsub('-', '_').to_sym}")
  # tsNow
  # end

  # Compute a hash-code for this OpenStruct.
  def hash
    to_h.hash
  end

  # Retrieves the value object corresponding to the each +name+
  # objects repeatedly.
  def dig(name, *names)
    begin
      name = name.to_sym
    rescue NoMethodError
      raise(TypeError, "#{name} is not a symbol nor a string")
    end
    to_h.dig(name, *names)
  end
end

class Ok < OkMain
  class << self
    def jsonFileCanvas(hIn)
      Ok.new(Oj.load(File.read((hIn[:jsonFile]).to_s)))
    end
  end
end
