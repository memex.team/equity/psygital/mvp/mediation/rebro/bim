# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

class Listen
  # Only used for WebSocket or SSE upgrades.
  def call(env)
    if env['rack.upgrade?'].nil?
      [404, {}, []]
    else
      env['rack.upgrade'] = $clock
      [200, {}, []]
    end
  end
end

Agoo::Server.handle(:GET, '/upgrade', Listen.new)
