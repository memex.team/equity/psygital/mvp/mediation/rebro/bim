# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
module ReBro
  module API
    module Entity
      class << self
        def getParams(hIn)
          res = {}
          hIn.each do |e, eProps|
            res[e] = {}
            eProps.each do |k, v|
              case k
              when :is_array, :required
                next
              else
                res[e][k] = v
              end
            end
          end
          res
        end

        def getDesc(hIn)
          res = {}
          hIn.each do |e, eProps|
            res[e] = {}
            eProps.each do |k, v|
              res[e][k] = v
            end
            res[e][:param_type] = 'body'
          end
          res
        end

        def genClasses(hIn)
          %w[Desc Params].each do |className|
            classContent =
              Class.new(Grape::Entity) do
                h = ReBro::API::Entity.send("get#{className}".to_sym, hIn[:fields])
                h.each do |k, v|
                  expose k, documentation: v
                end
              end
            (hIn[:path]).to_s.constantize.send(:const_set, className, classContent)
          end
        end

        def resolveScope(hIn)
          entityClass = hIn[:ns].split('::')
          entityClassEnding = entityClass[-1]
          entityClass.pop
          entityClass << 'Entity'
          entityClass << entityClassEnding

          res = {}
          res[:desc] = "#{entityClass.join('::')}::#{hIn[:scope].capitalize}::Desc".constantize
          res[:params] = "#{entityClass.join('::')}::#{hIn[:scope].capitalize}::Params".constantize
          res
        end
      end
    end
  end
end
