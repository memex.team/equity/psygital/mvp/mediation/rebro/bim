# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module ReBro
  module API
    module Linter
      class << self
        def description(hIn)
          res = {}

          apiClasses = hIn[:class].constants
          apiClasses.delete(:Entity) if apiClasses.include?(:Entity)
          apiClasses.each do |className|
            "#{hIn[:class]}::#{className}".constantize.routes.each do |route|
              a = []

              a << :description if route.description == ''   or route.description.nil?
              a << :summary if route.options[:summary] == '' or route.options[:summary].nil?
              a << :detail  if route.options[:detail] == ''  or route.options[:detail].nil?
              a << :entity  if route.options[:entity] == ''  or route.options[:entity].nil?
              a << :tags    if route.options[:tags].nil?
              a << :params  if route.options[:params] == {}

              next unless a.count > 0

              res[className] ||= {}
              res[className][route.namespace] ||= {}
              res[className][route.namespace][route.app.options[:path][0]] ||= {}
              res[className][route.namespace][route.app.options[:path][0]][route.app.options[:method][0]] = a
            end
          end

          res
        end

        def stdout(hIn)
          if hIn[:linter].length > 0
            hIn[:linter].each do |_className, classNameProps|
              classNameProps.each do |ns, nsParams|
                nsParams.each do |path, pathParams|
                  pathParams.each do |method, fields|
                    pp("   @@ Incorrect API spec: fields '#{fields}' of '#{method} #{path}' (namespace #{ns})")
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
