# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

RACK_ENV = (ENV['RACK_ENV'] || 'development').to_sym

require './lib/sys/storables/ok'
class O < Ok; end
require './lib/sys/addons/uuid'
def appbasePath
  "#{File.dirname(File.expand_path(__FILE__))}/../.."
end

def basePathString
  appBase = File.dirname(File.expand_path(__FILE__)).to_s.split('/')
  appBase.pop
  appBase.pop
  appBase.pop
  appBase.join('/')
end

def corePathString
  appBase = File.dirname(File.expand_path(__FILE__)).to_s.split('/')
  appBase.pop
  appBase.pop
  appBase.join('/')
end

def reBroPath
  "#{basePathString}/streams/#{ENV['reBroInstance']}"
end
#
# Bundler.require

def GrapeErrorHandler(e, output)
  trace = []
  i = 0
  e.backtrace.each do |tracePath|
    trace << "APPPATH#{tracePath.delete_prefix!(basePathString)}" if i < 34
    i += 1
  end

  begin
    eName = e.name
  rescue StandardError
    eName = ''
  end
  err = { problemIn: eName, errorMessage: e.message, 'backtrace (last 14)': trace }

  res = false
  case output
  when :response
    res = err
  else
    Log::App.error(false, err)
  end

  res
end

module Env
  module Base
    class << self
      def streamResolve
        res = {}
        Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/1_deck/**", '*.rb')).each do |f|
          f = f.split('/')
          n0 = f[3]
          n1 = f[4]
          n2 = f[5].split('_')[1]
          n3 = f[6]
          n4 = f[6]
          res[n0] ||= {}
          res[n0][n1] ||= {}
          res[n0][n1][n2] ||= {}
          res[n0][n1][n2][n3] ||= []
          res[n0][n1][n2][n3].push(n4.split('.')[0])
        end
        res
      end

      def broResolve
        res = {}
        Dir.glob(File.join('./lib/bro/*/*', '*.rb')).each do |f|
          f = f.split('/')
          n0 = f[3]
          n1 = f[4]
          n2 = f[5]
          res[n0] ||= {}
          res[n0][n1] ||= []
          res[n0][n1].push(n2.split('.')[0])
        end
        res
      end
    end
  end
end
