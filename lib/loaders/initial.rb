# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

def autoBundler
  File.open('.bundle/config', 'r').each do |f|
    next unless f =~ /BUNDLE_WITH/

    f.split('"')[1].split(':').each do |group|
      Bundler.require(group.to_sym)
    end
  end
end

def generateStreamModules
  streamResolve = Env::Base.streamResolve
  # Log::App.debug(false, streamResolve)
  streamResolve.each do |n0, n0Params|
    Object.class_eval(<<-CUT, __FILE__, __LINE__ + 1)
        module Env
          module #{n0.capitalizeFirst}
          end
        end
    CUT

    n0Params.each do |n1, _n1Params|
      Object.class_eval(<<-CUT, __FILE__, __LINE__ + 1)
        module Stream
          module #{n0.capitalizeFirst}
            module #{n1.capitalizeFirst}
            end
          end
        end
      CUT
    end
  end
end

def generateBroModules
  broResolve = Env::Base.broResolve
  broResolve.each do |n0, n0Params|
    n0Params.each do |n1, _n1Params|
      Object.class_eval(<<-CUT, __FILE__, __LINE__ + 1)
        module Bro
          module #{n0.capitalizeFirst}
            module #{n1.capitalizeFirst}
            end
          end
        end
      CUT
    end
  end
end

def includeReBro
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/", 'config.rb')).each            { |f| require(f) }
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/", 'env.rb')).each           { |f| require(f) }
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/1_deck/**", '*.rb')).each    { |f| require(f) }
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/2_cables/**", '*.rb')).each  { |f| require(f) }
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/3_pylons/**", '*.rb')).each  { |f| require(f) }
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/4_bro/**", '*.rb')).each     { |f| require(f) }
  Dir.glob(File.join("../streams/#{ENV['reBroInstance']}/*/*/5_data/**", '*.rb')).each    { |f| require(f) }
end
