# frozen_string_literal: true

#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module ReBro
  module API
    module Base
      class Root < Grape::API
        content_type :json, 'application/json'
        content_type :html, 'text/html'
        format :json

        rescue_from :all do |e|
          res = GrapeErrorHandler(e, :response)
          error!(res, 500)
        end
        # use ElasticAPM::Middleware

        # Auto
        streamResolve = Env::Base.streamResolve
        # Log::App.debug(false, streamResolve)
        streamResolve.each do |n0, n0Params|
          n0Params.each do |n1, n1Params|
            n1Params.each do |n2, n2Params|
              n2Params.each do |n3, _n3Params|
                className = "Stream::#{n0.capitalizeFirst}::#{n1.capitalizeFirst}::#{n2.capitalizeFirst}::#{n3.capitalizeFirst}"
                apiPath = "#{Env::Base::API.apiPrefix._path}/#{n0.downcase}/#{n1.downcase}/#{n3.downcase}"
                Log::App.info('API path', apiPath)
                # Log::App.info(false, className)
                #
                mount(className.constantize => apiPath)
              end
            end
          end
        end

        add_swagger_documentation format: :json,
                                  info: Env::Base::API.apiDesc,
                                  mount_path: "#{Env::Base::API.apiPrefix._path}/oapi",
                                  schemes: %w[http https]
      end
    end
  end
end
