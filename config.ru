# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2022 Eugene Istomin
#   Copyright © 2017-2022 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######
#
require 'date'
timeStart = DateTime.now.strftime('%Q').to_i
#
require 'rubygems'
require 'agoo'

require 'bundler'
require 'bundler/setup'
require 'bundler/cli'
require 'bundler/cli/info'

Bundler.require(:default)
#
require 'openssl'
#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE  if ENV.key?('ReBroSslVerify') && ENV['ReBroSslVerify'] == 'none'
#
require './lib/loaders/core'
require './lib/sys/grape/basic'

###################
# 1
Dir.glob(File.join("./lib/sys/**", "*.rb")).each {|f| require f}
# 2
Grape.configure do |config|
  config.param_builder = Grape::Extensions::Canvas::ParamBuilder
end
# 3
require './lib/loaders/initial'
# 4
autoBundler
# 5
generateStreamModules 
# 6
generateBroModules 
# 7
Dir.glob(File.join("./lib/bro/**", "*.rb")).each {|f| require f}
# 8
includeReBro
# 9
require './lib/loaders/grapeApiConstructor'
# 10
ReBro::API::Base::Root.compile!
# 11
#ReBro::API::Linter.stdout(linter: ReBro::API::Linter.description(class: ReBro::Env::Base.api[:class]))
# 12
GC::Profiler.enable
# 13
#ElasticAPM::Grape.start(ReBro::API, ReBro::Env.apm)
# 14
run ReBro::API::Base::Root
timeEnd = DateTime.now.strftime('%Q').to_i
pp "Started in #{((timeEnd.to_f - timeStart.to_f)/1000).round(5)} seconds"
###################




#################  DEV  ####################
# use Rack::Cors do
#   allow do
#     origins '*'
#     resource '*', headers: :any, methods: %i[get post put patch delete options]
#   end
# end


# use Rack::Static,
#   :urls => ["/images", "/lib", "/js", "/css"],
#   :root => "public/swagger_ui"
# 
# map '/swagger-ui' do
#   run lambda { |env|
#     [
#       200,
#       {
#         'Content-Type'  => 'txt-html',
#         'Cache-Control' => 'public, max-age=86400'
#       },
#       File.open('public/swagger_ui/index.html', File::RDONLY)
#     ]
#   }
# end


#require 'rack/unreloader'
#Unreloader = Rack::Unreloader.new{Api}
#require 'grape'
#Unreloader.require './api/_start'
#run Unreloader

#################  DEV  ####################
